<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function profile()
    {
        return $this->hasOne('App\Profile', 'user_id');
    }


    public function comments()
    {
        return $this->hasMany('App\Comment', 'user_id');
    }

    public function posts()
    {
        return $this->belongsToMany('App\Post', 'post_user', 'user_id', 'post_id')->withPivot('jumlah_like')->withTimestamps();
    }

    public function comment()
    {
        return $this->belongsToMany('App\Comment', 'comment_user', 'user_id', 'comment_id')->withPivot('jumlah_like')->withTimestamps();
    }

    public function follow()
    {
        return $this->belongsToMany('App\User', 'user_user', 'user_id', 'user_follow_id')->withTimestamps();
    }
}
