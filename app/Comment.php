<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $table = "comment";
    protected $guarded = [];
    protected $primaryKey = 'comment_id';
    
    public function user()
    {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }

    public function post()
    {
        return $this->belongsTo('App\Post', 'post_id', 'post_id');
    }

    public function users()
    {
        return $this->belongsToMany('App\User', 'comment_user','comment_id','user_id')->withPivot('jumlah_like');
    }
}
