<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{

    // protected $fillable = ['nama_lengkap','foto','user_id'];
    protected $guarded = [];

    protected $table = 'profile';
    protected $primaryKey = 'profile_id';

    public function profile()
    {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }
}
