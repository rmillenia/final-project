<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Auth;
Use App\Comment;
use App\User;

class CommentController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function store(Request $request)
    {
        $request->validate([
            'isi' => 'required',
            'post_id' => 'required'
        ]);

        $comment = Comment::create([
            'isi' => $request["isi"],
            'post_id' => $request["post_id"],
            'user_id'=> Auth::id()         
        ]);

        return redirect('/home')->with('success', 'Berhasil Upload!');
    }

    public function destroy($id)
    {
        // $query = DB::table('post')->where('post_id', $id)->delete();
        Comment::destroy($id);
        return redirect('/home')->with('success', 'Post berhasil dihapus!');
    }

    public function like(Request $request)
    {
        $user = Auth::user();
        // dd($request['post_id']);
        // dd($user);
        $hasLike = $user->comment()->where('comment.comment_id', $request['comment_id'])->exists();
        $comment = Comment::find($request['comment_id']);
        if($hasLike == false){
            $user->comment()->attach($request['comment_id'],['jumlah_like' => 1]);
            $hasLike == true;
        }
        return redirect('/home')->with('success', 'Post berhasil dihapus!')->with('hasLike', $hasLike);
    }
}
