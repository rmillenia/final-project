<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Post;
use App\User;
use Auth;

class PostController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function create()
    {
        return view('post.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'file' => 'mimes:jpeg,png,jpg,gif,svg|required',
            'caption' => 'required',
            'quote' => 'required'
        ]);

        $imgName = $request->file->getClientOriginalName() . '-' . time() . '.' . $request->file->extension();
        $request->file->move(public_path('image'), $imgName);

        // $query = DB::table('post')->insert([
        //     'caption' => $request["caption"],
        //     'quote' => $request["quote"]
        // ]);

        $post = Post::create([
            'foto' => $imgName,
            'caption' => $request["caption"],
            'quote' => $request["quote"],
            'user_id' => Auth::id()
        ]);
        return redirect('/post')->with('success', 'Berhasil Upload!');





        // $post = Post::create([
        //     // "file" => $request["file"],
        //     "caption" => $request["caption"],
        //     "quote" => $request["quote"]
        // ]);

        // return redirect('/posts')->with('success', 'Post berhasil disimpan!');
    }

    public function index()
    {
        // $post = DB::table('post')->get();
        $post = Post::all();
        // dd($post);
        return view('post.index', compact('post'));
    }

    public function show($id)
    {
        // $post = DB::table('post')->where('post_id', $id)->first();
        $post = Post::find($id);
        return view('post.show', compact('post'));
    }

    public function edit($id)
    {
        // $post = DB::table('post')->where('post_id', $id)->first();
        $post = Post::find($id);
        return view('post.edit', compact('post'));
    }

    public function update($id, Request $request)
    {
        $request->validate([
            // 'file' => 'required',
            'caption' => 'required',
            'quote' => 'required'
        ]);

        // $query = DB::table('post')
        //     ->where('post_id', $id)
        //     ->update([
        //         // 'file' => $request['file'],
        //         'caption' => $request['caption'],
        //         'quote' => $request['quote']
        //     ]);


        $update = Post::where('post_id', $id)->update([

            'caption' => $request['caption'],
            'quote' => $request['quote']
        ]);

        return redirect('/post')->with('toast_success', 'Post sudah di update!');
    }

    public function destroy($id)
    {
        // $query = DB::table('post')->where('post_id', $id)->delete();
        Post::destroy($id);
        return redirect('/post')->with('success', 'Post berhasil dihapus!');
    }

    public function like(Request $request)
    {
        $user = Auth::user();
        // dd($request['post_id']);
        // dd($user);
        $hasLike = $user->posts()->where('post.post_id', $request['post_id'])->exists();
        // $post = Post::find($request['post_id']);
        if ($hasLike == false) {
            $user->posts()->attach($request['post_id'], ['jumlah_like' => 1]);
            $hasLike == true;
        }
        return redirect('/home')->with('success', 'Berhasil difollow!')->with('hasLike', $hasLike);
    }



    // function pdf()
    // {
    //     // $data['judul'] = "Data Table";
    //     $pdf = \PDF::loadView('post.index');
    //     return $pdf->download('invoice.pdf');
    // }
}
