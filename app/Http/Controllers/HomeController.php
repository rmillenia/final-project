<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use App\User;
use Auth;
use DB;



class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $post = Post::all();
        return view('home', compact('post'));
    }

    public function profileOther($id)
    {
        $user = Auth::user($id);
        // dd($user);
        $post = Post::find($id);
        $post1 = Post::where(['user_id' => $id])->get();

        // $countFollowers = Auth::user();
        $countFollowing = DB::table('user_user')
            ->select('user_user.user_id as id')
            ->join('users', 'user_user.user_id', '=', 'users.id')
            ->join('users as b', 'user_user.user_follow_id', '=', 'b.id')
            ->where('user_user.user_id', $id)
            ->get();

        // dd($countFollowing);

        $countFollowers = DB::table('user_user')
            ->select('user_user.user_follow_id as id')
            ->join('users', 'user_user.user_id', '=', 'users.id')
            ->join('users as b', 'user_user.user_follow_id', '=', 'b.id')
            ->where('user_user.user_follow_id', $id)
            ->get();

        // dd($countFollowers);


        return view('showProfil')->with(compact('user'))->with(compact('post'))->with(compact('post1'))->with(compact('countFollowers'))->with(compact('countFollowing'));
        // return view('showProfil');
    }



    public function follow(Request $request)
    {
        $user = Auth::user();
        // dd($request['post_id']);
        // dd($user);
        $hasLike = $user->follow()->where('users.id', $request['users.id'])->exists();
        // dd($hasLike);
        // $find = User::find($request['user_id']);
        // dd($find);
        if ($hasLike == false && $request['user_id'] != Auth::id()) {
            $user->follow()->attach($request['user_id']);
            $hasLike == true;
        }
        $id = $request['user_id'];
        return redirect('/profileOther/' . $id)->with('success', 'Post berhasil dihapus!')->with('hasLike', $hasLike);
    }

    function pdf()
    {
        $data['judul'] = "Laporan PDF";
        $pdf = \PDF::loadView('pdf', $data);
        return $pdf->download('invoice.pdf');
    }
}
