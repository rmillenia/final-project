<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Profile;
use Auth;

class ProfileController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }


    public function index()
    {
        $profile = Profile::where('user_id', Auth::id())->get();
        return view('profile.profil', compact('profile'));
    }


    // public function edit($id)
    // {
    //     $profile = Profile::findOrFail($id);
    //     return view('profile.edit', compact('profile'));
    // }

    public function update($id, Request $request)
    {
        $request->validate([
            'name' => 'required'
        ]);

        // $update = Profile::find($id);
        // $update->nama_lengkap = 'tes';
        // $update->bio = $request['bio'];
        // $update->save();

        $imgName = $request->file->getClientOriginalName() . '-' . time() . '.' . $request->file->extension();
        $request->file->move(public_path('image'), $imgName);

        $update = Profile::where('profile_id', $id)->update([

            'nama_lengkap' => $request['name'],
            'foto' => $imgName,
            'bio' => $request['bio']

        ]);
        // dd($request);

        return redirect('/profile')->with('success', 'Data berhasil di update!');
    }
}
