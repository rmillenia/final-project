<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class Post extends Model
{
    protected $table = "post";
    protected $guarded = [];
    protected $primaryKey = 'post_id';

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }

    public function comments()
    {
        return $this->hasMany('App\Comment', 'post_id');
    }

    public function users()
    {
        return $this->belongsToMany('App\User', 'post_user','post_id','user_id')->withPivot('jumlah_like');
    }
}
