@extends('adminMaster.master')

@section('content')
<div class="row">
    {{-- {{$profile}} --}}
    <div class=" col-md-4">
        <section class="card">
            <div class="twt-feed blue-bg">
                <div class="corner-ribon black-ribon">
                    <i class="fa fa-twitter"></i>
                </div>
                <div class="fa fa-twitter wtt-mark"></div>

                <div class="media">
                    <a href="#">
                        <img class="align-self-center rounded-circle mr-3" style="width:85px; height:85px;" alt="" src="{{asset('adminMaster/images/admin.jpg')}}">
                    </a>
                    <div class="media-body">
                        <h2 class="text-white display-6">{{Auth::user()->profile->nama_lengkap }}</h2>
                        <p class="text-light">{{Auth::user()->profile->bio }}</p>
                    </div>
                </div>



            </div>
            <div class="weather-category twt-category">
                <ul>
                    <li class="active">
                        <h5>750</h5>
                        Like
                    </li>
                    <li>
                        <h5>865</h5>
                        Follower
                    </li>
                    <li>
                        <h5>3645</h5>
                        Dislike
                    </li>
                </ul>
            </div>
            <div class="row ml-3">
                <div class="ml-5">
                    <h4>email : </h4>
                    <p></p>
                </div>
                <div class="ml-5">
                    <h4>pasword : </h4>
                    <p></p>
                </div>


            </div>
            <footer class="twt-footer">
                <a href="#"><i class="fa fa-camera"></i></a>
                <a href="#"><i class="fa fa-map-marker"></i></a>
                New Castle, UK
                <span class="pull-right">
                    32
                </span>
            </footer>
        </section>
    </div>
    <!--form edit-->
    <div class="col-md-8 bg-light">
        <div>
            <form action="/profile/{{Auth::user()->profile->profile_id}}" method="POST" enctype="multipart/form-data">
                @csrf
                @method('PUT')
                <h3 class="mt-2" align="center">Update Profile {{Auth::user()->profile->nama_lengkap }}</h3>
                <!--nama-->
                <div class="form-group">
                    <label for="title">Nama</label>
                    <input type="text" class="form-control" name="name" value="{{old('name', Auth::user()->profile->nama_lengkap) }}" id="name" placeholder="Masukkan Nama">
                    @error('name')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                    @enderror
                </div>

                <!--biodata-->
                <div class="form-group">
                    <label for="body">Biodata</label>
                    <input type="text" class="form-control" name="bio" value="{{old('bio', Auth::user()->profile->bio)}}" id="bio" placeholder="Masukkan biodata">
                    @error('bio')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                    @enderror
                </div>
                <!--foto-->
                <div class="form-group">
                    <div class="col-12 col-md-9"><input type="file" id="file-input" name="file" value="{{Auth::user()->profile->foto}}" class="form-control-file"></div>
                    @error('file')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                    @enderror
                </div>

                <button type="submit" class="btn btn-primary">Simpan</button>
                <a href="/" class="btn btn-danger">Kembali</a>
            </form>

        </div>
    </div>
</div>
@endsection