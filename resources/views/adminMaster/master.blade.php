@include('adminMaster.bagian.palingAtas')

<body>


    <!-- Left Panel -->

    @include('adminMaster.bagian.sidebar')

    <!-- Left Panel -->

    <!-- Right Panel -->

    <div id="right-panel" class="right-panel">

        <!-- Header-->
        @include('adminMaster.bagian.header')
        <!-- Header-->

        <div class="content mt-3">

            <!-- isi -->
            @yield('content')


            <!-- isi -->



        </div> <!-- .content -->
    </div><!-- /#right-panel -->

    <!-- Right Panel -->

    <script src="{{asset('adminMaster/vendors/jquery/dist/jquery.min.js')}}"></script>
    <script src="{{asset('adminMaster/vendors/popper.js/dist/umd/popper.min.js')}}"></script>
    <script src="{{asset('adminMaster/vendors/bootstrap/dist/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('adminMaster/assets/js/main.js')}}"></script>


    <script src="{{asset('adminMaster/vendors/chart.js/dist/Chart.bundle.min.js')}}"></script>
    <script src="{{asset('adminMaster/assets/js/dashboard.js')}}"></script>
    <script src="{{asset('adminMaster/assets/js/widgets.js')}}"></script>
    <script src="{{asset('adminMaster/vendors/jqvmap/dist/jquery.vmap.min.js')}}"></script>
    <script src="{{asset('adminMaster/vendors/jqvmap/examples/js/jquery.vmap.sampledata.js')}}"></script>
    <script src="{{asset('adminMaster/vendors/jqvmap/dist/maps/jquery.vmap.world.js')}}"></script>
    <script>
        (function($) {
            "use strict";

            jQuery('#vmap').vectorMap({
                map: 'world_en',
                backgroundColor: null,
                color: '#ffffff',
                hoverOpacity: 0.7,
                selectedColor: '#1de9b6',
                enableZoom: true,
                showTooltip: true,
                values: sample_data,
                scaleColors: ['#1de9b6', '#03a9f5'],
                normalizeFunction: 'polynomial'
            });
        })(jQuery);
    </script>
    @stack('script')
    @include('sweetalert::alert')
</body>

</html>