@extends('adminMaster.master')

@section('content')
<div class="content mt-3">
    <div class="animated fadeIn">
        <div class="row">

            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <strong class="card-title ">Data Table</strong><br>

                    </div>
                    <!--card header-->
                    <div class="card-body mb-3">
                        @if(session('success'))
                        <div class="alert alert-success">
                            {{session('success')}}
                        </div>
                        @endif
                        <table id="bootstrap-data-table-export" class="table table-striped table-bordered">
                            <a href="/post/create" class="btn btn-primary"><i class="fa fa-plus-square"></i>&nbsp; Tambah</a>
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <!-- <th>Name</th> -->
                                    <th>gambar</th>
                                    <th>caption</th>
                                    <th>quote</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse($post as $key => $post)
                                <tr>
                                    <td>{{$key + 1}}</td>
                                    <td>
                                        <img src="/image/{{$post->foto}}" alt="" width="100" height="100">
                                    </td>
                                    <td>{{$post->caption}}</td>
                                    <td>{{$post->quote}}</td>
                                    <td style="display: flex;">
                                        <a href="/post/{{$post->post_id}}" class="btn btn-info">Show</a>
                                        <a href="/post/{{$post->post_id}}/edit" class="btn btn-primary">Edit</a>
                                        <form action="/post/{{$post->post_id}}" method="POST">
                                            @csrf
                                            @method('DELETE')
                                            <input type="submit" class="btn btn-danger" value="Delete">
                                        </form>
                                    </td>
                                </tr>
                                @empty
                                <tr>
                                    <td colspan="5" align="center">No Post</td>
                                </tr>
                                @endforelse
                                <!-- <tr>
                                    <td>Tiger Nixon</td>
                                    <td>System Architect</td>
                                    <td>Edinburgh</td>
                                    <td>
                                        <a href="" class="btn btn-info">Show</a>
                                        <a href="/post/edit" class="btn btn-primary">Edit</a>
                                        <form action="/posts/delete" method="POST">
                                            @csrf
                                            @method('DELETE')
                                            <input type="submit" class="btn btn-danger my-1" value="Delete">
                                        </form>

                                    </td>
                                </tr> -->


                            </tbody>
                        </table>
                    </div>
                </div>
            </div>


        </div>
    </div><!-- .animated -->
</div><!-- .content -->

@endsection
@push('script')
<script src="{{asset('adminMaster/vendors/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('adminMaster/vendors/datatables.net-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('adminMaster/vendors/datatables.net-buttons/js/dataTables.buttons.min.js')}}"></script>
<script src="{{asset('adminMaster/vendors/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js')}}"></script>
<script src="{{asset('adminMaster/vendors/jszip/dist/jszip.min.js')}}"></script>
<script src="{{asset('adminMaster/vendors/pdfmake/build/pdfmake.min.js')}}"></script>
<script src="{{asset('adminMaster/vendors/pdfmake/build/vfs_fonts.js')}}"></script>
<script src="{{asset('adminMaster/vendors/datatables.net-buttons/js/buttons.html5.min.js')}}"></script>
<script src="{{asset('adminMaster/vendors/datatables.net-buttons/js/buttons.print.min.js')}}"></script>
<script src="{{asset('adminMaster/vendors/datatables.net-buttons/js/buttons.colVis.min.js')}}"></script>
<script src="{{asset('adminMaster/assets/js/init-scripts/data-table/datatables-init.js')}}"></script>
@endpush