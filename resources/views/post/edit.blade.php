@extends('adminMaster.master')

@section('content')
<h2>Edit Postingan</h2>
<form action="/post/{{$post->post_id}}" method="POST" enctype="multipart/form-data" class="alert">
    @csrf
    @method('PUT')
    <!--foto-->
    <div class="custom-file mb-3">
        <input type="file" class="custom-file-input" id="file" name="file">
        <label class="custom-file-label" for="customFile">Masukkan Gambar</label>
    </div>
    <!--caption-->
    <div class="form-group">
        <label for="body">caption</label>
        <input type="text" class="form-control" name="caption" id="caption" value="{{old('caption', $post->caption)}}" placeholder="Masukkan Body">
        @error('caption')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
        @enderror
    </div>
    <!--quote-->
    <div class="form-group">
        <label for="body">Quote</label>
        <input type="text" class="form-control" name="quote" id="quote" value="{{old('quote', $post->quote)}}" placeholder="Masukkan Body">
        @error('quote')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
        @enderror
    </div>

    <button type="submit" class="btn btn-primary ">Edit</button>
</form>
</div>
@endsection
