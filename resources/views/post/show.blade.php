@extends('adminMaster.master')

@section('content')
<div class="container">
    <div class="row ml-5">
        <div class="col-md-12">

            <div class="card">
                <div class="card-header">
                    <strong class="card-title"><a href="">Zainur Ridho</a></strong>
                </div>
                <img class="card-img-top" src="/image/{{$post->foto}}" alt="Card image cap">
                <div class="card-body">
                    <h4 class="card-title mb-3">Card Image Title</h4>
                    <p class="card-text">{{ $post->caption }}</p>
                    <p class="card-text">{{$post->quote}}</p>
                    <div class="weather-category twt-category">
                        <ul>
                            <a href="">
                                <li class="active" id="like">
                                    <i class="fa fa-thumbs-up"></i>
                                    <h5>750</h5>
                                </li>
                            </a>
                            <a href="">
                                <li class="active" >
                                    <i class="fa  fa-comment"></i>
                                    <h5>750</h5>
                                </li>
                            </a>

                            <li>
                                <i class="fa fa-thumbs-down"></i>
                                <h5>865</h5>
                            </li>
                        </ul>
                    </div>
                    <div class="twt-write col-sm-12">
                        <textarea placeholder="Write your Tweet and Enter" rows="1" class="form-control t-text-area"></textarea>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection