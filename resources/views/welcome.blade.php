<!doctype html>
<html class="no-js" lang="zxx">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Sanber Media</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="manifest" href="site.webmanifest">
    <link rel="shortcut icon" type="image/x-icon" href="assets/img/favicon.ico">

    <!-- CSS here -->
    <link rel="stylesheet" href="{{asset('courseMaster/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('courseMaster/css/owl.carousel.min.css')}}">
    <link rel="stylesheet" href="{{asset('courseMaster/css/slicknav.css')}}">
    <link rel="stylesheet" href="{{asset('courseMaster/css/flaticon.css')}}">
    <link rel="stylesheet" href="{{asset('courseMaster/css/progressbar_barfiller.css')}}">
    <link rel="stylesheet" href="{{asset('courseMaster/css/gijgo.css')}}">
    <link rel="stylesheet" href="{{asset('courseMaster/css/animate.min.css')}}">
    <link rel="stylesheet" href="{{asset('courseMaster/css/animated-headline.css')}}">
    <link rel="stylesheet" href="{{asset('courseMaster/css/magnific-popup.css')}}">
    <link rel="stylesheet" href="{{asset('courseMaster/css/fontawesome-all.min.css')}}">
    <link rel="stylesheet" href="{{asset('courseMaster/css/themify-icons.css')}}">
    <link rel="stylesheet" href="{{asset('courseMaster/css/slick.css')}}">
    <link rel="stylesheet" href="{{asset('courseMaster/css/nice-select.css')}}">
    <link rel="stylesheet" href="{{asset('courseMaster/css/style.css')}}">
    
</head>

<body>

    <header>
        <!-- Header Start -->
        <div class="header-area header-transparent">
            <div class="main-header ">
                <div class="header-bottom  header-sticky">
                    <div class="container-fluid">
                        <div class="row align-items-center">
                            <!-- Logo -->
                            <div class="col-xl-2 col-lg-2 ">
                                <div class="logo">
                                    <h1 style="color: white; padding: 20px;">Sanber Media</h1>
                                </div>
                            </div>
                            <div class="col-xl-10 col-lg-10">
                                <div class="menu-wrapper d-flex align-items-center justify-content-end">
                                    <!-- Main-menu -->
                                    <div class="main-menu d-none d-lg-block">
                                        <nav>
                                            @if (Route::has('login')) 
                                            <ul id="navigation">  
                                                                                                                                      
                                                <!-- Button -->
                                                @auth
                                                <li class="button-header margin-left "><a href="{{ url('/home') }}" class="btn">home</a></li>
                                                @else
                                                <li class="button-header"><a href="{{ route('login') }}" class="btn btn3">Login</a></li>
                                                @if (Route::has('register'))
                                                <li class="button-header"><a href="{{ route('register') }}" class="btn btn3">Register</a></li>
                                                @endif
                                                 @endauth
                                
                                            </ul>
                                            @endif
                                        </nav>
                                    </div>
                                </div>
                            </div> 
                            <!-- Mobile Menu -->
                            <div class="col-12">
                                <div class="mobile_menu d-block d-lg-none"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Header End -->
    </header>
    <main>
        <!--? slider Area Start-->
        <section class="slider-area ">
            <div class="slider-active">
                <!-- Single Slider -->
                <div class="single-slider slider-height d-flex align-items-center">
                    <div class="container">
                        <div class="row">
                            <div class="col-xl-6 col-lg-7 col-md-12">
                                <div class="hero__caption">
                                    <h1 data-animation="fadeInLeft" data-delay="0.2s">Sanber Media</h1>
                                    <p data-animation="fadeInLeft" data-delay="0.4s">Rasakan hari yang menyenangkan dengan sanber media</p>
                                    <a href="{{ route('login') }}" class="btn hero-btn" data-animation="fadeInLeft" data-delay="0.7s">Login</a>
                                </div>
                            </div>
                        </div>
                    </div>          
                </div>
            </div>
        </section>

<!-- JS here -->
<script src="{{asset('sourceMaster/js/vendor/modernizr-3.5.0.min.js')}}"></script>
<!-- Jquery, Popper, Bootstrap -->
<script src="{{asset('sourceMaster/js/vendor/jquery-1.12.4.min.js')}}"></script>
<script src="{{asset('sourceMaster/js/popper.min.js')}}"></script>
<script src="{{asset('sourceMaster/js/bootstrap.min.js')}}"></script>
<!-- Jquery Mobile Menu -->
<script src="{{asset('sourceMaster/js/jquery.slicknav.min.js')}}"></script>

<!-- Jquery Slick , Owl-Carousel Plugins -->
<script src="{{asset('sourceMaster/js/owl.carousel.min.js')}}"></script>
<script src="{{asset('sourceMaster/js/slick.min.js')}}"></script>
<!-- One Page, Animated-HeadLin -->
<script src="{{asset('sourceMaster/js/wow.min.js')}}"></script>
<script src="{{asset('sourceMaster/js/animated.headline.js')}}"></script>
<script src="{{asset('sourceMaster/js/jquery.magnific-popup.js')}}"></script>

<!-- Date Picker -->
<script src="{{asset('sourceMaster/js/gijgo.min.js')}}"></script>
<!-- Nice-select, sticky -->
<script src="{{asset('sourceMaster/js/jquery.nice-select.min.js')}}"></script>
<script src="{{asset('sourceMaster/js/jquery.sticky.js')}}"></script>
<!-- Progress -->
<script src="{{asset('sourceMaster/js/jquery.barfiller.js')}}"></script>

<!-- counter , waypoint,Hover Direction -->
<script src="{{asset('sourceMaster/js/jquery.counterup.min.js')}}"></script>
<script src="{{asset('sourceMaster/js/waypoints.min.js')}}"></script>
<script src="{{asset('sourceMaster/js/jquery.countdown.min.js')}}"></script>
<script src="{{asset('sourceMaster/js/hover-direction-snake.min.js')}}"></script>

<!-- contact js -->
<script src="./assets/js/contact.js"></script>
<script src="./assets/js/jquery.form.js"></script>
<script src="./assets/js/jquery.validate.min.js"></script>
<script src="./assets/js/mail-script.js"></script>
<script src="./assets/js/jquery.ajaxchimp.min.js"></script>

<!-- Jquery Plugins, main Jquery -->	
<script src="./assets/js/plugins.js"></script>
<script src="./assets/js/main.js"></script>

</body>
</html>