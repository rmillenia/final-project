@extends('adminMaster.master')

@section('content')
<div class="container">
    <div class="row ml-5">
        <div class="col-md-12">

            <!--awal card-->
            @foreach($post as $key)
            <div class="card">
                <div class="card-header">
                    <strong class="card-title"><a href="/profileOther/{{$key->user->id}}">{{$key->user->username}}</a></strong>
                </div>
                <br>
                <center><img class="card-img-top" src="/image/{{$key->foto}}" alt="Card image cap" style="width: 25%; height: auto;"></center>
                <div class="card-body">
                 <h4 class="card-title mb-3">Caption</h4>

                 <p>{{$key->caption}}</p>
                 <p>{{$key->quote}}</p>
                 <div class="weather-category twt-category" style="text-align: center;">
                    <ul>
                        <a href="">
                            <li class="active">
                             <i class="fa fa-thumbs-up"></i>
                                <h5>{{$key->users->count()}}</h5>
                             @foreach($key->users as $hasil)
                             {{-- {{$hasil}} --}}
                             @if($hasil->pivot->post_id == $key->post_id && $hasil->pivot->user_id == Auth::id())
                             @push('script')
                             <script type="text/javascript">
                                jQuery("#like{{$hasil->pivot->post_id}}").prop("disabled", true);
                                var x = document.getElementById("info{{$hasil->pivot->post_id}}");
                                x.style.display = "block";
                            </script>
                            @endpush
                            @endif
                            @endforeach
                                  {{--  @if($key->users-)
                                   {{ $array_to_search['keyname'] }}
                                   @endif  --}}
                               </li>
                           </a>
                           <a href="">
                            <li class="active">
                                <i class="fa  fa-comment"></i>
                                <h5>{{$key->comments->count()}}</h5>
                            </li>
                        </a>
                    </ul>
                </div>
                <div  style="text-align: center;">
                    <form action="/likePost" method="POST">
                        @csrf
                        <input type="hidden" name="post_id" id="post_id" value="{{$key->post_id}}">
                        <input type="submit" class="btn btn-warning" value="Like" id="like{{$key->post_id}}" name="like">
                    </form>
                    <div id="info{{$key->post_id}}" style="display: none;"><p><br>You already liked this</p></div>
                </div>

                <div class="twt-write col-sm-12">
                    <h4 class="card-title mb-3">Comment</h4>



                    <form action="/comment" method="POST">
                        @csrf
                        <div class="col-9">
                            <input type="hidden" name="post_id" id="post_id" value="{{$key->post_id}}">

                            <textarea placeholder="Write your Comment" rows="1" class="form-control t-text-area" id="isi" name="isi"></textarea>
                        </div>
                        <div class="col-1">
                        </div>

                        <div class="col-2">
                            <button type="submit" class="btn btn-primary float-right">Tambah</button>
                        </div>
                    </form>
                    <br><br>

                    {{-- {{$key->comments->users}} --}}
                    @forelse ($key->comments as $key => $value)
                    <div class="col-9">
                      <p><b>{{$value->user->username}} : </b> {{$value->isi}}</p> 
                  </div>
                  <div class="col-1" style="padding: 6px">
                    <h5><i class="fa fa-thumbs-up" id="tumbs{{$value->comment_id}}"></i> {{$value->users->count()}}</h5>
                        @foreach($value->users as $hasil)
                             {{-- {{$hasil}} --}}
                             @if($hasil->pivot->comment_id == $value->comment_id && $hasil->pivot->user_id == Auth::id())
                             @push('script')
                             <script type="text/javascript">
                                jQuery("#liked{{$hasil->pivot->comment_id}}").prop("disabled", true);
                                document.getElementById("tumbs{{$hasil->pivot->comment_id}}").style.color = "pink";
                            </script>
                            @endpush
                            @endif
                        @endforeach
                </div>
                <div class="col-1">
                    <form action="/comment/{{$value->comment_id}}" method="POST">
                        @csrf
                        @method('DELETE')
                        <input type="submit" class="btn btn-danger" value="Delete">
                    </form>
                </div>
                <div class="col-1">
                    <form action="/likeComment" method="POST">
                        @csrf
                        <input type="hidden" name="comment_id" id="comment_id" value="{{$value->comment_id}}">
                        <input type="submit" class="btn btn-warning" value="Like" id="liked{{$value->comment_id}}" name="like">
                    </form>
                </div>
                @empty
                @endforelse
            </div>
        </div>
    </div>
    @endforeach
</div>
</div>
</div>
@endsection