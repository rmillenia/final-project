@extends('adminMaster.master')

@section('content')
<div class="row">
    <div class=" col-md-12">
        <section class="card">
            <div class="twt-feed blue-bg">
                <div class="corner-ribon black-ribon">
                    @foreach($user->follow as $hasil)
                        @if($hasil->pivot->user_follow_id == $post->user->id && $hasil->pivot->user_id == Auth::id() || $hasil->pivot->user_id == Auth::id())
                            @push('script')
                            <script type="text/javascript">
                                var x = document.getElementById("follows");
                                x.style.display = "none";
                                var y = document.getElementById("info");
                                y.style.display = "block";
                            </script>
                            @endpush
                        @endif
                    @endforeach
                    <form action="/follow" method="POST">
                        @csrf
                        <input type="hidden" name="user_id" id="user_id" value="{{$post->user_id}}">
                        <a href="javascript:;" onclick="parentNode.submit();" id="follows"><i class="fa fa-plus"></i></a>
                        <input type="hidden" name="mess" value="Like" id="follow" name="follow">
                    </form>
                </div>
                <div class="fa fa-twitter wtt-mark"></div>

                <div class="media">
                    <a href="#">
                        <img class="align-self-center rounded-circle mr-3" style="width:85px; height:85px;" alt="" src="{{asset('adminMaster/images/admin.jpg')}}">
                    </a>
                    <div class="media-body">
                        <h1 class="text-white display-6">{{$post->user->username}}</h1>
                        <p class="text-light">{{$post->user->profile->nama_lengkap}}</p>
                    </div>
                </div>



            </div>
            <div class="weather-category twt-category" style="text-align: center;"> 
                <ul>
                    <li>
                        <h5>{{$countFollowers->count()}}</h5>
                        Followers
                    </li>
                    <li>
                        <h5>{{$countFollowing->count()}}</h5>
                        Following
                    </li>
                </ul>
            </div>
            <div id="info" style="display: none;"><center>You already follow this user</center></div>
            <div class="row ml-3">
                <footer class="twt-footer">
                    <a href="#"><i class="fa fa-camera"></i></a>
                    <a href="#"><i class="fa fa-map-marker"></i></a>
                    New Castle, UK
                    <span class="pull-right">
                        32
                    </span>
                </footer>
            </section>
        </div>
        <div class="col-md-12">
            {{-- {{$post}} --}}
             @foreach($post1 as $hasil => $key)
                <div class="card col-md-4">
                    <br>
                    <center><strong class="card-title">Post {{$hasil+1}}</strong></center>
                    <div class="card-body">
                        <center><img class="card-img-top" src="/image/{{$key->foto}}" alt="Card image cap" style="width: 50%; height: 120px;"></center>
                    </div>
                </div>
                @endforeach
        </div>
    </div>
    @endsection