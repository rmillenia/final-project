<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// Route::get('/', function () {
//     return view('post.home');
// });
// ROute::get('/beranda', function () {
//     return view('post.home');
// });
// Route::get('/post', function () {
//     return view('post.index');
// });

Route::get('/post', 'PostController@index');
Route::get('/post/create', 'PostController@create');
Route::post('/post', 'PostController@store');
Route::get('/post/{id}', 'PostController@show');
Route::get('/post/{id}/edit', 'PostController@edit');
Route::put('/post/{id}', 'PostController@update');
Route::delete('/post/{id}', 'PostController@destroy');

Route::post('/likePost', 'PostController@like');
Route::post('/likeComment', 'CommentController@like');

Route::get('/post/update', function () {
    return view('post.update');
});

//profil
Route::get('/profile', 'ProfileController@index');
Route::put('/profile/{id}', 'ProfileController@update');

// home
Route::get('/login', function () {
    return view('post.login');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/profileOther/{id}', 'HomeController@profileOther');
Route::post('/follow', 'HomeController@follow');

Route::post('/comment', 'CommentController@store');
Route::delete('/comment/{id}', 'CommentController@destroy');

//laravel excel
Route::get('export-excel', 'UsersController@export');

//pdf
Route::get('/pdf', 'HomeController@pdf');
